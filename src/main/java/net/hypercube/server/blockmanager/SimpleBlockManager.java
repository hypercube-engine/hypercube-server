package net.hypercube.server.blockmanager;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.joml.Vector3i;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.api.blocks.AbstractBlock;
import net.hypercube.api.blocks.IBlockManager;
import net.hypercube.api.worldgenerators.IRequiredAlias;

public class SimpleBlockManager implements IBlockManager {
	final static Logger log = LoggerFactory.getLogger(SimpleBlockManager.class);

	private final Map<Integer, Class<? extends AbstractBlock>> registeredBlocks;
	private final Map<IRequiredAlias, Class<? extends AbstractBlock>> registeredAliases;

	public SimpleBlockManager() {
		this.registeredBlocks = new HashMap<>();
		this.registeredAliases = new HashMap<>();
	}

	@Override
	public Integer registerBlock(final Class<? extends AbstractBlock> blockClass) {
		log.trace("Registering block class {}", blockClass);

		// TODO: Use getId() somehow
		Integer internalId = blockClass.getCanonicalName().hashCode();
		registeredBlocks.put(internalId, blockClass);

		log.debug("Registered block class as {}", internalId);
		return internalId;
	}

	@Override
	public void registerAlias(Class<? extends AbstractBlock> clazz, IRequiredAlias alias) {
		log.trace("Registering alias for block class {}", clazz);

		if (registeredAliases.containsKey(alias)) {
			log.warn(String.format("Overriding alias %s with %s", alias, clazz));
		}

		registeredAliases.put(alias, clazz);

		log.debug("Registered block class alias");
	}

	@Override
	public Class<? extends AbstractBlock> getBlockClassById(int id) {
		return registeredBlocks.get(id);
	}

	@Override
	public Class<? extends AbstractBlock> getBlockClassByAlias(IRequiredAlias alias) throws NoSuchElementException {
		if (registeredAliases.containsKey(alias)) {
			return registeredAliases.get(alias);
		}

		throw new NoSuchElementException("Could not find" + alias);
	}

	@Override
	public AbstractBlock instantiate(Class<? extends AbstractBlock> clazz, Vector3i position) {
		log.trace("Instantiating block at {} as {}", position, clazz.getName());
		AbstractBlock instance = null;

		try {
			instance = clazz.getConstructor(Vector3i.class).newInstance(position);
			log.debug("Instantiated block");
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			log.error("Failed to instantiate block at {}: {}", position, e.getMessage());
		}

		return instance;
	}
}

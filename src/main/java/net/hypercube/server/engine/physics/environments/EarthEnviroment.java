package net.hypercube.server.engine.physics.environments;

/**
 * A physical environment representing the conditions on Earth.
 * 
 * @author Christian Danscheid
 */
public final class EarthEnviroment extends AbstractPhysicalEnvironment {
	public EarthEnviroment() {
		super(5.972E24, 6.371E6);
	}
}

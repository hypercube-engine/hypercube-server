package net.hypercube.server.engine.physics;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

import org.joml.Vector3d;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.server.engine.ICoreEngine;
import net.hypercube.server.engine.ISpecializedEngine;
import net.hypercube.server.engine.physics.environments.AbstractPhysicalEnvironment;

/**
 * A physics engine
 * 
 * @author Christian Danscheid
 */
public final class PhysicsEngine implements ISpecializedEngine {
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(getClass());

	private final ICoreEngine coreEngine;
	private final AbstractPhysicalEnvironment environment;

	public PhysicsEngine(ICoreEngine coreEngine, AbstractPhysicalEnvironment environment) {
		this.coreEngine = coreEngine;
		this.environment = environment;
	}

	@Override
	public int getTargetTickRate() {
		return 30;
	}

	private final Predicate<Object> physical = (gameObject) -> gameObject instanceof Physical;

	private final Function<Object, Physical> castPhysical = (object) -> (Physical) object;

	@Override
	public void step(double dtime, Collection<Object> gameObjects) {
		gameObjects.stream().filter(physical).map(castPhysical).forEach((gameObject) -> {
			applyGravity(gameObject, environment);

			handleKineticEnergy(gameObject, dtime);
			updateWorldPosition(gameObject, dtime);

			cleanUp(gameObject);
		});
	}

	/**
	 * Apply gravity to the object
	 */
	protected static void applyGravity(Physical object, AbstractPhysicalEnvironment environment) {
		var force = new Vector3d(0, -PhysicsUtils.getWeight(environment, object), 0);
		object.applyForce(force, object.getPotentialEnergy());
	}

	/**
	 * Updates object's kinetic energy based on forces attached to it
	 */
	protected static void handleKineticEnergy(Physical object, double dtime) {
		object.getActingForces().entrySet().stream().forEach((entry) -> {
			var force = entry.getKey();
			var energySource = entry.getValue();
			var requiredWork = PhysicsUtils.mechanicalWork(force.length(), dtime, object.getMass());
			energySource.transfer(requiredWork, object.getKineticEnergy());
			object.getNormalizedDirection().add(force);
		});
	}

	/**
	 * Updates object's world position based on its kinetic energy
	 */
	protected static void updateWorldPosition(Physical object, double dtime) {
		var scalarVelocity = Math.sqrt(2 * object.getKineticEnergy().getAmount() / object.getMass());
		var vectorVelocity = object.getNormalizedDirection().mul(scalarVelocity, new Vector3d());
		var deltaPosition = vectorVelocity.mul(dtime, new Vector3d());
		object.setWorldPosition(object.getWorldPosition().add(deltaPosition));
	}

	/**
	 * Prepares the object for the next frame
	 */
	protected static void cleanUp(Physical physicalObject) {
		physicalObject.getActingForces().clear();
	}
}

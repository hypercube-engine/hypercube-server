package net.hypercube.server.gameobjects.illumination;

import org.joml.Vector3d;

import net.hypercube.api.gameobjects.illumination.DirectionalLighting;
import net.hypercube.api.gameobjects.illumination.IlluminationType;

public class DirectionalLight implements DirectionalLighting {
	private Vector3d direction;

	public DirectionalLight(Vector3d direction) {
		this.direction = direction;
	}

	@Override
	public final IlluminationType getType() {
		return IlluminationType.DIRECTIONAL;
	}

	@Override
	public final Vector3d getDirection() {
		return direction;
	}

	@Override
	public final void setDirection(Vector3d direction) {
		this.direction = direction;
	}
}

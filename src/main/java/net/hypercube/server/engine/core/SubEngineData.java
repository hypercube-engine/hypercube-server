package net.hypercube.server.engine.core;

import net.hypercube.server.engine.ISpecializedEngine;

final class SubEngineData {
	public final ISpecializedEngine engine;
	public final int tickRate;
	public long lastCall;

	private SubEngineData(ISpecializedEngine engine) {
		this.engine = engine;
		this.tickRate = engine.getTargetTickRate();
		this.lastCall = System.currentTimeMillis();
	}

	public static SubEngineData of(ISpecializedEngine engine) {
		return new SubEngineData(engine);
	}
}

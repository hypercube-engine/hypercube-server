package net.hypercube.server.engine.physics;

import net.hypercube.server.engine.physics.environments.AbstractPhysicalEnvironment;

public final class PhysicsUtils {
	private PhysicsUtils() {
	}

	/**
	 * Get object weight
	 * 
	 * @param environment
	 * @param object
	 * @return weight in [N]
	 */
	public static double getWeight(AbstractPhysicalEnvironment environment, Physical object) {
		return object.getMass() * environment.getGravitationalAcceleration(object.getWorldPosition().y);
	}

	/**
	 * Calculate mechanical work
	 * 
	 * @param force The force in [N]
	 * @param time  The time for the force to act in [s]
	 * @param mass  The mass of the object the force acts on in [kg]
	 * @return work in [J]
	 */
	public static double mechanicalWork(final double force, final double time, final double mass) {
		return (force * force * time * time) / mass;
	}

	/**
	 * Calculate mechanical work
	 * 
	 * @param force    The force in [N]
	 * @param distance The distance on which the force is acting in [m]
	 * @return work in [J]
	 */
	public static double mechanicalWork(final double force, final double distance) {
		return force * distance;
	}
}

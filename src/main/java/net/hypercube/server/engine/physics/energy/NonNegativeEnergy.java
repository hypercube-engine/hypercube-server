package net.hypercube.server.engine.physics.energy;

import java.util.Optional;

/**
 * An implementation of physical energy following the law of conservation of
 * energy that does not allow the energy to go below 0. Once initialized, the
 * contained energy cannot be destroyed or created out of nowhere.
 * 
 * @author Christian Danscheid
 *
 */
public class NonNegativeEnergy extends Energy {
	public NonNegativeEnergy() {
		super();
	}

	/**
	 * @param amount An amount of energy in [J], must be > 0
	 */
	public NonNegativeEnergy(final double amount) {
		super(Optional.of(amount).filter((value) -> value > 0).orElseThrow(IllegalArgumentException::new));
	}

	/**
	 * Reduce energy, but don't allow it to go below 0.
	 * 
	 * @param amount
	 * @return
	 */
	@Override
	protected double take(double amount) {
		return super.take((this.amount > amount) ? amount : this.amount);
	}
}

package net.hypercube.server.engine.physics;

import java.util.Map;

import org.joml.Vector3d;

import net.hypercube.server.engine.physics.energy.Energy;
import net.hypercube.server.engine.physics.energy.NonNegativeEnergy;

/**
 * A physical object must have a density, volume and position.
 * 
 * @author chris
 *
 */
public interface Physical {
	/**
	 * The object's density
	 * 
	 * @return density in [kg/m³]
	 */
	double getDensity();

	/**
	 * The object's volume
	 * 
	 * @return volume in [m³]
	 */
	double getVolume();

	/**
	 * The object's mass
	 * 
	 * @return mass in [kg]
	 */
	default double getMass() {
		return getDensity() * getVolume();
	}

	NonNegativeEnergy getKineticEnergy();

	NonNegativeEnergy getChemicalEnergy();

	Energy getPotentialEnergy();

	/**
	 * Get the height of the object relative to the planet surface.
	 * 
	 * @return
	 */
	Vector3d getWorldPosition();

	void setWorldPosition(Vector3d position);

	/**
	 * List of forces acting on this object
	 * 
	 * @return Set of {@link Vector3d}, each in [N]
	 */
	Map<Vector3d, Energy> getActingForces();

	/**
	 * Apply a force to the object that acts on it during the next tick
	 * 
	 * @param force  in [N]
	 * @param source An {@link Energy} to take energy from if the force causes work
	 */
	void applyForce(Vector3d force, Energy source);

	Vector3d getNormalizedDirection();

	/**
	 * Get current veclocity
	 * 
	 * @return veclocity in [m/s]
	 */
	default Vector3d getVelocity() {
		return getNormalizedDirection().mul(Math.sqrt((2 * getKineticEnergy().getAmount()) / getMass()),
				new Vector3d());
	}
}

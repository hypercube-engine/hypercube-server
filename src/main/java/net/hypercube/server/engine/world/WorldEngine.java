package net.hypercube.server.engine.world;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import org.joml.Vector3i;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.api.chunks.ChunkUtils;
import net.hypercube.api.gameobjects.entities.Player;
import net.hypercube.server.engine.ICoreEngine;
import net.hypercube.server.engine.ISpecializedEngine;

public class WorldEngine implements ISpecializedEngine {
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(getClass());

	private final IWorldManager worldManager;

	private final ICoreEngine coreEngine;

	public WorldEngine(ICoreEngine coreEngine, IWorldManager worldManager) {
		this.worldManager = worldManager;
		this.coreEngine = coreEngine;
	}

	@Override
	public int getTargetTickRate() {
		return 2;
	}

	@Override
	public void step(double dtime, Collection<Object> gameObjects) {
		final var chunksToAdd = new HashSet<>();
		final var chunksToRemove = new HashSet<>();

		gameObjects.stream().filter((gameObject) -> gameObject instanceof Player).forEach((gameObject) -> {
			var player = (Player) gameObject;

			var chunkOfPlayer = ChunkUtils.chunkPositionFromWorldPosition(player.getAbsoluteTranslation(),
					worldManager.getWorldGenerator().getChunkEdgeLength());

			getNeighboringChunks(chunkOfPlayer, 1).forEach((chunk) -> chunksToAdd.add(worldManager.getChunk(chunk)));
		});

		coreEngine.addGameObjects(chunksToAdd);
		coreEngine.removeGameObjects(chunksToRemove);
	}

	private Set<Vector3i> getNeighboringChunks(Vector3i chunkPos, int radius) {
		var chunks = new HashSet<Vector3i>();

		IntStream.range(-radius, radius + 1).forEach((x) -> {
			IntStream.range(-radius, radius + 1).forEach((y) -> {
				IntStream.range(-radius, radius + 1).forEach((z) -> {
					chunks.add(chunkPos.add(new Vector3i(x, y, z), new Vector3i()));
				});
			});
		});

		return chunks;
	}
}

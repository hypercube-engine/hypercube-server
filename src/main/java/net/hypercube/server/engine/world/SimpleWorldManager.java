package net.hypercube.server.engine.world;

import java.util.HashMap;
import java.util.Map;

import org.joml.Vector3i;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.api.chunks.Chunk;
import net.hypercube.api.worldgenerators.IWorldGenerator;

public class SimpleWorldManager implements IWorldManager {

	final static Logger log = LoggerFactory.getLogger(SimpleWorldManager.class);

	private IWorldGenerator worldGenerator;

	private Map<Vector3i, Chunk> chunkCache;

	public SimpleWorldManager(IWorldGenerator worldGenerator) {
		log.trace("Initializing with worldGenerator {}", worldGenerator);
		this.worldGenerator = worldGenerator;
		this.chunkCache = new HashMap<>();
	}

	@Override
	public IWorldGenerator getWorldGenerator() {
		return worldGenerator;
	}

	@SuppressWarnings("unused")
	@Override
	public Chunk getChunk(Vector3i position) {
		log.trace("Searching Chunk at {}", position);

		if (chunkCache.containsKey(position)) {
			log.trace("Chunk has already been cached, returning this instance");
			return chunkCache.get(position);
		} else {
			if (false) {
				log.trace("Chunk has already been generated");
				return null;
			}

			log.trace("Chunk needs to be generated");

			var chunk = worldGenerator.generateChunk(position);
			chunkCache.put(position, chunk);

			log.debug("Found Chunk at {}", position);
			return chunk;
		}
	}

}

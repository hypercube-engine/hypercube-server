package net.hypercube.server.engine.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.api.blocks.IBlockManager;
import net.hypercube.api.gameobjects.entities.Entity;
import net.hypercube.api.gameobjects.entities.Player;
import net.hypercube.api.games.IGame;
import net.hypercube.eventmanager.EventManager;
import net.hypercube.server.engine.ICoreEngine;
import net.hypercube.server.engine.ISpecializedEngine;
import net.hypercube.server.engine.physics.PhysicsEngine;
import net.hypercube.server.engine.physics.environments.SpaceEnvironment;
import net.hypercube.server.engine.world.IWorldManager;
import net.hypercube.server.engine.world.WorldEngine;

public final class CoreEngine implements ICoreEngine {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private final Set<Object> gameObjects = new HashSet<>();

	private final List<SubEngineData> subEngines = new ArrayList<>();

	private final EventManager eventManager;
	private final IGame game;
	private final IBlockManager blockManager;
	private final IWorldManager worldManager;

	private double lastCall;
	private double currentDtime;

	public CoreEngine(EventManager eventManager, IGame game, IBlockManager blockManager, IWorldManager worldManager) {
		this.game = game;
		this.blockManager = blockManager;
		this.worldManager = worldManager;
		this.eventManager = eventManager;

		currentDtime = 0d;
	}

	@Override
	public void addGameObject(Object gameObject) {
		gameObjects.add(gameObject);
	}

	@Override
	public void addGameObjects(Set<Object> gameObjects) {
		this.gameObjects.addAll(gameObjects);
	}

	@Override
	public void removeGameObject(Object gameObject) {
		gameObjects.remove(gameObject);
	}

	@Override
	public void removeGameObjects(Set<Object> gameObjects) {
		this.gameObjects.removeAll(gameObjects);
	}

	@Override
	public void initialize() {
		log.info("CoreEngine is initializing");

		addSubEngine(new PhysicsEngine(this, new SpaceEnvironment()));
		addSubEngine(new WorldEngine(this, worldManager));

		initializeSubEngines();

		gameObjects.addAll(game.getWorldLights());

		Player player = game.onPlayerJoining("");

		gameObjects.add(player);

		log.info("CoreEngine initialized successfully");
	}

	private void addSubEngine(ISpecializedEngine engine) {
		subEngines.add(SubEngineData.of(engine));
	}

	private void initializeSubEngines() {
		subEngines.parallelStream().forEach((subEngineData) -> subEngineData.engine.initialize());
	}

	@Override
	public void step() {
		dispatchEvents();
		calculateDeltaTime();
		subengineStep();
		entityStep(currentDtime);
	}

	private void dispatchEvents() {
		eventManager.dispatchAll();
	}

	private void calculateDeltaTime() {
		var currentTimeMillis = System.currentTimeMillis();
		currentDtime = (currentTimeMillis - lastCall) / 1e3;
		lastCall = currentTimeMillis;
	}

	private void subengineStep() {
		subEngines.stream().filter(subEngineShouldTick).forEach((subEngineData) -> {
			var currentTimeMillis = System.currentTimeMillis();
			final var delta = (currentTimeMillis - subEngineData.lastCall) / 1e3;
			subEngineData.lastCall = currentTimeMillis;
			doStep(subEngineData, gameObjects.stream(), delta);
		});
	}

	private void entityStep(double dtime) {
		gameObjects.parallelStream().filter((gameObject) -> gameObject instanceof Entity).forEach((gameObject) -> {
			((Entity) gameObject).onStep(dtime);
		});
	}

	/**
	 * Whether it's time for a subEngine to step
	 */
	private final Predicate<SubEngineData> subEngineShouldTick = (
			subEngineData) -> (subEngineData.lastCall + (1 / subEngineData.tickRate) * 1e3) <= System
					.currentTimeMillis();

	/**
	 * Helper method for letting a subEngine step
	 * 
	 * @param subEngineData    The subEngine metadata
	 * @param gameObjectStream Stream of gameObjects
	 */
	private void doStep(SubEngineData subEngineData, Stream<Object> gameObjectStream, double dtime) {
		subEngineData.lastCall = System.currentTimeMillis();

		subEngineData.engine.step(dtime, gameObjects);
	}

	@Override
	public void shutDown() {
		log.info("CoreEngine is shutting down");

		subEngines.parallelStream().forEach((subEngineData) -> {
			subEngineData.engine.shutDown();
			subEngines.remove(subEngineData);
		});

		gameObjects.clear();

		log.info("CoreEngine shut down successfully");
	}
}

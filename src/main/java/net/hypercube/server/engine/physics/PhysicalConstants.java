package net.hypercube.server.engine.physics;

/**
 * This class bundles physical constants in a central place.
 * 
 * @author Christian Danscheid
 *
 */
public final class PhysicalConstants {
	/**
	 * The gravity constant in [m³/(kg * s²)]
	 */
	public static final double GRAVITATIONAL_CONSTANT = 6.67408E-11;

	/**
	 * The speed of light in a vacuum in [m/s]
	 */
	public static final double SPEED_OF_LIGHT = 299792458;

	/**
	 * The elementary charge in [C]
	 */
	public static final double ELEMENTARY_CHARGE = 1.6021766208E-19;

	/**
	 * Absolute zero of temperature in [K]
	 */
	public static final double ABSOLUTE_ZERO = 0;

	/**
	 * Magnetic constant in [N/A²]
	 */
	public static final double MAGNETIC_CONSTANT = Math.PI * 10E-7;

	/**
	 * Electric constant in [A*s/V*m]
	 */
	public static final double ELECTRIC_CONSTANT = Math.pow(MAGNETIC_CONSTANT * Math.pow(SPEED_OF_LIGHT, 2), -1);

	/**
	 * Coulomb constant in [A*s/V*m]
	 */
	public static final double COULOMB_CONSTANT = Math.pow((4 * Math.PI * ELECTRIC_CONSTANT), -1);
}

package net.hypercube.server.gameobjects.illumination;

import net.hypercube.api.gameobjects.illumination.AmbientLighting;
import net.hypercube.api.gameobjects.illumination.IlluminationType;

public class AmbientLight implements AmbientLighting {
	private float intensity;

	public AmbientLight(float intensity) {
		this.intensity = intensity;
	}

	@Override
	public final IlluminationType getType() {
		return IlluminationType.AMBIENT;
	}

	@Override
	public final float getIntensity() {
		return intensity;
	}

	@Override
	public final void setIntensity(float intensity) {
		this.intensity = intensity;
	}
}

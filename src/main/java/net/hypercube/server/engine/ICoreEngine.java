package net.hypercube.server.engine;

import java.util.Set;

public interface ICoreEngine {
	default void initialize() {
	}

	default void shutDown() {
	}

	void step();

	void addGameObject(Object gameObject);

	void addGameObjects(Set<Object> gameObjects);

	void removeGameObject(Object gameObject);

	void removeGameObjects(Set<Object> gameObjects);
}

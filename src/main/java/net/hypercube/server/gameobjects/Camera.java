package net.hypercube.server.gameobjects;

import org.joml.Vector3d;

public class Camera {
	public Vector3d location;

	public Vector3d direction;

	public Camera(final Vector3d location, final Vector3d direction) {
		this.location = location;
		this.direction = direction;
	}

	public Vector3d getLocation() {
		return location;
	}

	public Vector3d getDirection() {
		return direction;
	}
}

package net.hypercube.server.engine.physics.environments;

import net.hypercube.server.engine.physics.PhysicalConstants;

/**
 * An AnstractPhysicalEnvironment simulates a planet.
 * 
 * @author Christian Danscheid
 *
 */
public abstract class AbstractPhysicalEnvironment {
	private final double planetMass;

	/**
	 * Distance between gravitational center point and the planet's surface in [m]
	 */
	private final double verticalOffset;

	/**
	 * @param planetMass     Mass of the planet to simulate in [kg]
	 * @param verticalOffset Distance between gravitational center and planet
	 *                       surface, aka planet radius in [m]
	 */
	public AbstractPhysicalEnvironment(double planetMass, double verticalOffset) {
		this.planetMass = planetMass;
		this.verticalOffset = verticalOffset;
	}

	/**
	 * Get the gravitational acceleration for a given height
	 * 
	 * @param surfaceRelativeHeight Height relative to the planet's surface [m]
	 * @return acceleration [m/s²]
	 */
	public double getGravitationalAcceleration(double surfaceRelativeHeight) {
		return PhysicalConstants.GRAVITATIONAL_CONSTANT * planetMass
				/ Math.pow(verticalOffset + surfaceRelativeHeight, 2);
	}
}

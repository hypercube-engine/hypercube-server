package net.hypercube.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.eventmanager.EventManager;
import net.hypercube.server.engine.ICoreEngine;
import net.hypercube.server.engine.core.CoreEngine;

public final class HyperCubeServer {
	private static final Logger log = LoggerFactory.getLogger(HyperCubeServer.class);

	private boolean serverShouldStop = false;
	private ICoreEngine coreEngine;

	public static void main(String[] args) {
		var server = new HyperCubeServer();

		server.initialize();
		server.run();
		server.destroy();
	}

	public void initialize() {
		log.info("Initializing HyperCube server");
		coreEngine = new CoreEngine(new EventManager(), null, null, null);
		coreEngine.initialize();
	}

	public void run() {
		log.info("HyperCube server is now running");
		while (!serverShouldStop) {
			coreEngine.step();
		}
	}

	public void destroy() {
		log.info("HyperCube server is shutting down");
		coreEngine.shutDown();
	}
}

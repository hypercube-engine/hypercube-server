package net.hypercube.server.engine.physics.energy;

/**
 * An implementation of physical energy following the law of conservation of
 * energy. Once initialized, the contained energy cannot be destroyed or created
 * out of nowhere.
 * 
 * @author Christian Danscheid
 *
 */
public class Energy {
	/**
	 * Energy in [J]
	 */
	protected double amount;

	public Energy() {
		this.amount = 0;
	}

	/**
	 * @param amount An amount of energy in [J], must be > 0
	 */
	public Energy(final double amount) {
		this.amount = amount;
	}

	/**
	 * Get amount of energy
	 * 
	 * @return energy in [J]
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * Reduce energy
	 * 
	 * @param amount
	 * @return
	 */
	protected double take(final double amount) {
		this.amount -= amount;
		return amount;
	}

	/**
	 * Increase energy
	 * 
	 * @param amount
	 */
	protected void add(final double amount) {
		this.amount += amount;
	}

	/**
	 * Transfer a certain amount of energy from this energy to another one.
	 * 
	 * @param amount      The amount to transfer in [J]; Must be > 0. If this energy
	 *                    has less than you want to transfer, the actually available
	 *                    energy will be transferred.
	 * @param destination The energy transfer partner. Energy can only be
	 *                    transformed to another energy, you cannot create or
	 *                    destroy it.
	 * @return The amount of transferred energy.
	 */
	public double transfer(final double amount, final Energy destination) {
		var taken = this.take(amount);
		destination.add(taken);
		return taken;
	}
}

package net.hypercube.server.gameobjects;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import org.joml.Quaterniond;
import org.joml.Vector3d;

import net.hypercube.api.assets.material.Material;
import net.hypercube.api.assets.mesh.ExternalMesh;
import net.hypercube.api.assets.mesh.Mesh;
import net.hypercube.api.gameobjects.GameObject;
import net.hypercube.api.gameobjects.entities.Entity;
import net.hypercube.api.gameobjects.entities.Player;
import net.hypercube.server.engine.physics.Physical;
import net.hypercube.server.engine.physics.energy.Energy;
import net.hypercube.server.engine.physics.energy.NonNegativeEnergy;

public class PlayerEntity implements GameObject, Physical, Player {
	private static final Vector3d DEFAULT_DIRECTION = new Vector3d(0, -1, 0);
	private static final Vector3d DEFAULT_SCALE = new Vector3d(1, 1, 1);
	private static final Quaterniond DEFAULT_ROTATION = new Quaterniond();
	private static final Mesh DEFAULT_MESH = new ExternalMesh("assets/Models/character.obj",
			new Vector3d(.1f, .1f, .1f));

	private final String name;

	protected Mesh mesh;
	protected Material material;

	private final Vector3d worldPosition;
	protected final Vector3d scale;
	protected final Quaterniond rotation;
	protected final Vector3d direction;

	private final Map<Vector3d, Energy> forces;

	private final Vector3d velocity;

	protected final Set<Consumer<Entity>> subscribers = new HashSet<>();

	private final NonNegativeEnergy kineticEnergy;

	private final NonNegativeEnergy chemicalEnergy;

	private final Energy potentialEnergy;

	public PlayerEntity(String name, Vector3d worldPosition) {
		this.name = name;
		this.worldPosition = worldPosition;
		this.mesh = DEFAULT_MESH;
		this.scale = DEFAULT_SCALE;
		this.rotation = DEFAULT_ROTATION;
		this.direction = DEFAULT_DIRECTION;
		this.forces = new HashMap<>();
		this.velocity = new Vector3d(0, 0, 0);

		this.kineticEnergy = new NonNegativeEnergy();
		this.chemicalEnergy = new NonNegativeEnergy(100000);
		this.potentialEnergy = new Energy();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getDensity() {
		return 1060;
	}

	@Override
	public double getVolume() {
		return 0.075;
	}

	@Override
	public Vector3d getWorldPosition() {
		return worldPosition;
	}

	@Override
	public Vector3d getNormalizedDirection() {
		return direction.normalize();
	}

	@Override
	public Map<Vector3d, Energy> getActingForces() {
		return forces;
	}

	@Override
	public void applyForce(Vector3d force, Energy source) {
		forces.put(force, source);
	}

	@Override
	public Mesh getMesh() {
		return mesh;
	}

	@Override
	public final Material getMaterial() {
		return material;
	}

	@Override
	public final Vector3d getAbsoluteTranslation() {
		return mesh.getAbsoluteTranslation().add(worldPosition, new Vector3d());
	}

	@Override
	public void setTranslation(Vector3d translation) {
		this.worldPosition.x = translation.x;
		this.worldPosition.y = translation.y;
		this.worldPosition.z = translation.z;
		notifySubscribers();
	}

	@Override
	public void setWorldPosition(Vector3d position) {
		setTranslation(position);
	}

	@Override
	public final Vector3d getAbsoluteScale() {
		return mesh.getAbsoluteScale().mul(scale, new Vector3d());
	}

	@Override
	public void setScale(Vector3d scale) {
		if (scale == null)
			throw new IllegalArgumentException("Scale of entity " + this + " must not be null");

		this.scale.x = scale.x;
		this.scale.y = scale.y;
		this.scale.z = scale.z;

		notifySubscribers();
	}

	@Override
	public final Quaterniond getAbsoluteRotation() {
		return mesh.getAbsoluteRotation().mul(rotation, new Quaterniond());
	}

	@Override
	public final void setRotation(Quaterniond rotation) {
		if (rotation == null)
			throw new IllegalArgumentException("Rotation must not be null");

		this.rotation.x = rotation.x;
		this.rotation.y = rotation.y;
		this.rotation.z = rotation.z;
		this.rotation.w = rotation.w;

		notifySubscribers();
	}

	@Override
	public final void subscribe(Consumer<Entity> consumer) {
		subscribers.add(consumer);
	}

	@Override
	public final void unsubscribe(Consumer<Entity> consumer) {
		subscribers.remove(consumer);
	}

	@Override
	public final void notifySubscribers() {
		subscribers.parallelStream().forEach((subscriber) -> subscriber.accept(this));
	}

	@Override
	public Vector3d getVelocity() {
		return velocity;
	}

	@Override
	public void onStep(double dtime) {
	}

	@Override
	public NonNegativeEnergy getKineticEnergy() {
		return kineticEnergy;
	}

	@Override
	public NonNegativeEnergy getChemicalEnergy() {
		return chemicalEnergy;
	}

	@Override
	public Energy getPotentialEnergy() {
		return potentialEnergy;
	}
}

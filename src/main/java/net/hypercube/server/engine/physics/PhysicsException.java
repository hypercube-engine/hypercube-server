package net.hypercube.server.engine.physics;

public class PhysicsException extends Exception {
	private static final long serialVersionUID = 1L;

	public PhysicsException(String message) {
		super(message);
	}

	public PhysicsException(String message, Throwable cause) {
		super(message, cause);
	}
}

module net.hypercube.server {
	requires org.slf4j;
	requires org.apache.commons.collections4;
	requires joptsimple;
	requires transitive org.joml;

	requires net.hypercube.api;
	requires net.hypercube.eventmanager;

	exports net.hypercube.server.blockmanager;

	uses net.hypercube.api.worldgenerators.IWorldGenerator;
}
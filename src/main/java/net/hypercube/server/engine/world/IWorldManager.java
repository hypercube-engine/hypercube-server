package net.hypercube.server.engine.world;

import org.joml.Vector3i;

import net.hypercube.api.chunks.Chunk;
import net.hypercube.api.worldgenerators.IWorldGenerator;

public interface IWorldManager {
	Chunk getChunk(Vector3i position);

	IWorldGenerator getWorldGenerator();
}

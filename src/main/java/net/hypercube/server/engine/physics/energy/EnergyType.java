package net.hypercube.server.engine.physics.energy;

/**
 * Types of energy supported by the Physics engine
 * 
 * @author chris
 */
public enum EnergyType {
	KINETIC, CHEMICAL, ELECTRICAL, THERMAL, POTENTIAL;
}

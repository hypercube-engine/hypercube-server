package net.hypercube.server.engine;

import java.util.Collection;

import net.hypercube.server.engine.core.CoreEngine;

public interface ISpecializedEngine {
	/**
	 * Called before the first world step.
	 */
	default void initialize() {
	}

	/**
	 * The targeted tick rate of the engine in [Hz]
	 * 
	 * @return tick rate in [Hz]
	 */
	int getTargetTickRate();

	/**
	 * The engine step. The {@link CoreEngine} will try to call this at the rate
	 * provided by {@link #getTargetTickRate()}, however there is no guaranty that
	 * this rate will be reached!
	 * 
	 * @param dtime  Time since last call
	 * @param Object collection of all game objects managed by the
	 *               {@link CoreEngine}.
	 */
	void step(double dtime, Collection<Object> gameObjects);

	/**
	 * Called before the {@link CoreEngine} shuts down. Use for cleanup tasks.
	 */
	default void shutDown() {
	}
}
